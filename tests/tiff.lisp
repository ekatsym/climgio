(defpackage climgio/tests/main
  (:use :cl
        :climgio
        :rove))
(in-package :climgio/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :climgio)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
