(defsystem "climgio"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ()
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "util")
                 (:file "binary-data")
                 (:module "tiff"
                  :serial t
                  :components
                  ((:file "package")
                   (:file "core")
                   (:file "read")
                   (:file "write")))
                 (:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "climgio/tests"))))

(defsystem "climgio/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("climgio"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for climgio"
  :perform (test-op (op c) (symbol-call :rove :run c)))
