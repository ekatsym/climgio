(defpackage climgio.util
  (:use :cl)
  (:export #:natural #:index

           #:n-list?
           #:enum #:iter
           #:ensure-list #:proper-list->circular-list
           #:append1
           #:take #:drop #:insert

           #:sum #:list-sum

           #:ensure-keyword

           #:partial #:rpartial #:partial-n
           #:curry #:rcurry #:curry-n
           #:compose #:compose-n

           #:defmacro!

           #:for #:while #:until

           #:defclass!

           #:it #:alet #:aif
           ))
(in-package :climgio.util)

(declaim (inline drop list-sum append1))

;; type
(deftype natural ()
  '(integer 0 *))

(deftype index ()
  '(and fixnum (integer 0 *)))

;; list
(defun n-list? (n object)
  (declare (optimize (speed 3)))
  (check-type n index)
  (if (zerop n)
      (null object)
      (and (consp object)
           (n-list? (1- n) (rest object)))))

(defun enum (count &key (start 0) (step 1))
  (check-type count index)
  (do ((i count (1- i))
       (n start (+ n step))
       (acc '() (cons n acc)))
      ((zerop i) (nreverse acc))))

(defun iter (start next end &key (key #'identity))
  (check-type next function)
  (check-type end function)
  (check-type key function)
  (do ((x start (funcall next x))
       (acc '() (cons (funcall key x) acc)))
      ((funcall end x) (nreverse acc))))

(defun ensure-list (x)
  (if (listp x) x (list x)))

(defun proper-list->circular-list (proper-list)
  (let ((list (copy-list proper-list)))
    (do ((lst list (rest lst)))
        ((endp (rest lst))
         (setf (rest lst) list)
         list))))

(defun append1 (lst x)
  (append lst (list x)))

(defun take (lst n)
  (check-type lst list)
  (check-type n index)
  (do ((i n (1- i))
       (l lst (rest l))
       (acc '() (cons (first l) acc)))
      ((zerop i) (nreverse acc))))

(defun drop (lst n)
  (nthcdr n lst))

(defun insert (x lst n)
  (check-type n index)
  (check-type lst list)
  (append (take lst n) (cons x (drop lst n))))

;;; num
(defun sum (start end fn)
  (do ((i start (1+ i))
       (acc 0 (+ (funcall fn i) acc)))
      ((> i end) acc)))

(defun list-sum (lst)
  (reduce #'+ lst))

;;; keyword
(defun ensure-keyword (x)
  (intern (string x) :keyword))

;;; function
(defun partial (fn &rest args)
  (check-type fn function)
  (lambda (&rest rest-args) (apply fn (append args rest-args))))

(defun rpartial (fn &rest args)
  (check-type fn function)
  (lambda (&rest rest-args) (apply fn (append rest-args args))))

(defun partial-n (fn arg n)
  (check-type fn function)
  (check-type n index)
  (lambda (&rest args)
    (apply fn (insert arg args n))))

(defun curry (fn)
  (check-type fn function)
  (lambda (&rest args) (apply #'partial fn args)))

(defun rcurry (fn)
  (check-type fn function)
  (lambda (&rest args) (apply #'rpartial fn args)))

(defun curry-n (fn n)
  (check-type fn function)
  (check-type n index)
  (lambda (arg) (partial-n fn arg n)))

(defun compose (&rest fns)
  (declare (optimize (speed 3)))
  (cond ((endp fns) #'values)
        ((n-list? 1 fns) (let ((fn (first fns)))
                           (check-type fn function)
                           fn))
        (t (let ((fn (first fns))
                 (composed (apply #'compose (rest fns))))
             (check-type fn function)
             (lambda (&rest args)
               (declare (type function fn composed))
               (funcall fn (apply composed args)))))))

(defun compose-n (n fn)
  (declare (optimize (speed 3)))
  (check-type n index)
  (cond ((= n 0) #'values)
        ((= n 1) (check-type fn function)
                 fn)
        (t (let ((composed (compose-n (1- n) fn)))
             (lambda (&rest args)
               (declare (type function fn composed))
               (funcall fn (apply composed args)))))))

;;; defmacro
(defmacro defmacro/g! (name args &body body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (g!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "G!"
                         :start1 0 :end1 2))))

    (let ((syms (remove-duplicates
                  (remove-if-not #'g!-symbol?
                                 (flatten-defmacro! body)))))
      `(defmacro ,name ,args
         (let , (mapcar
                 (lambda (s)
                   `(,s (gensym ,(subseq
                                   (symbol-name s)
                                   2))))
                 syms)
           ,@body)))))

(defmacro defmacro! (name lambda-list &body body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (o!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "O!"
                         :start1 0 :end1 2)))

         (o!-symbol->g!-symbol (sym)
           (intern (concatenate 'string "G!" (subseq (symbol-name sym) 2)))))

    (let* ((os (remove-if-not #'o!-symbol?
                              (flatten-defmacro! lambda-list)))
           (gs (mapcar #'o!-symbol->g!-symbol os)))
      (if (null os)
        `(defmacro/g! ,name ,lambda-list ,@body)
        `(defmacro/g! ,name ,lambda-list
           `(let ,(mapcar #'list (list ,@gs) (list ,@os))
              ,(progn ,@body)))))))

;;; iterate macro
(defmacro! for ((var o!start o!end) &body body)
  `(if (<= ,g!start ,g!end)
       ;;; from g!start to g!end
       (do ((,var ,g!start (1+ ,var))
            (,g!limit ,g!end))
           ((> ,var ,g!limit))
           ,@body)
       ;;; from g!end to g!start
       (do ((,var ,g!start (1- ,var))
            (,g!limit ,g!end))
           ((< ,var ,g!limit))
           ,@body)))

(defmacro while (test &body body)
  `(do ()
       ((not ,test))
       ,@body))

(defmacro until (test &body body)
  `(do ()
       (,test)
       ,@body))

;;; define class
(defmacro! defclass! (name direct-superclasses &body direct-slots)
  `(defclass ,name ,direct-superclasses
     ,(mapcar #'defclass!-slot->slot direct-slots)))

(defun defclass!-slot->slot (slot)
  (cond ((listp slot)
         `(,(first slot) :initarg ,(ensure-keyword (first slot))
                         :accessor ,(first slot)
                         ,@(rest slot)))
        ((atom slot)
         `(,slot :initarg ,(ensure-keyword slot)
                 :accessor ,slot))))

;;; anaphora
(defmacro alet (form &body body)
  `(let ((it ,form))
     ,@body))

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))
