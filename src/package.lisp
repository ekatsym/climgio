(defpackage common-lisp-image-io-library
  (:nicknames climgio)
  (:use :common-lisp)
  (:export #:read-image
           #:write-image
           #:tiff))
