(defpackage climgio.main
  (:use :common-lisp)
  (:import-from :climgio
                #:read-image
                #:write-image
                #:tiff)
  (:import-from :climgio.tiff
                #:read-tiff
                #:write-tiff))
(in-package :climgio.main)

(defun read-image (stream format)
  (ecase format
    ((tiff)
     (read-tiff stream))))

(defun write-image (stream image format &key (depth 8))
  (ecase format
    ((tiff)
     (write-tiff stream image :depth depth))))
