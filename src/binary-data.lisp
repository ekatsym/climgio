(defpackage climgio.binary-data
  (:use :cl)
  (:import-from :climgio.util
                #:index
                #:sum
                #:for
                )
  (:export #:endian #:big-endian #:little-endian
           #:read-all-bytes #:write-all-bytes
           #:load-byte #:deposit-byte!))
(in-package :climgio.binary-data)

(deftype endian ()
  '(member big-endian little-endian))

(defun read-all-bytes (stream)
  (let* ((n (file-length stream))
         (result (make-array n)))
    (dotimes (i n result)
      (setf (svref result i)
            (read-byte stream)))))

(defun write-all-bytes (stream bytes)
  (dotimes (i (length bytes))
    (write-byte (svref bytes i) stream)))

(defun load-byte (bytes index size from-end)
  (declare (optimize (speed 3))
           (type index index size)
           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (let ((result 0))
    (dotimes (i size result)
      (setf (ldb (byte 8 (* i 8)) result)
            (svref bytes (if from-end
                                   (+ index i)
                                   (+ index (- size i 1))))))))

(defun deposit-byte! (bytes index size from-end value)
  (declare (optimize (speed 3))
           (type index index size value)
           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (dotimes (i size bytes)
    (setf (svref bytes (if from-end
                                 (+ index i)
                                 (+ index (- size i 1))))
          (ldb (byte 8 (* i 8)) value))))

(defsetf load-byte deposit-byte!)
