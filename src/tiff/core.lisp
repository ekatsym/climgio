(defpackage climgio.tiff.core
  (:use :common-lisp)
  (:import-from :climgio.util
                #:defclass!
                #:it #:aif)
  (:import-from :climgio.binary-data
                #:endian)
  (:import-from :climgio
                #:tiff)
  (:export #:tiff
           #:bytes #:ifds #:endian

           #:*ifd-tags

           #:ifd #:make-ifd
           #:image-width #:image-length #:bits/sample
           #:compression #:photometric-interpretation
           #:strip-offsets #:rows/strip #:strip-byte-counts
           #:x-resolution #:y-resolution #:resolution-unit
           #:color-map
           #:new-subfile-type #:thresholding
           #:cell-width #:cell-length #:fill-order
           #:image-description #:make #:model #:orientation
           #:samples/pixel #:min-sample-value
           #:max-sample-value #:planar-configuration
           #:gray-response-unit #:gray-response-curve
           #:software #:date-time #:artist #:host-computer
           #:extra-samples #:copyright
           #:unknown-tag

           #:ifd-entry #:make-ifd-entry
           #:ifd-tag #:ifd-type #:ifd-count #:ifd-data

           #:tag-name #:tag-code
           #:type-name #:type-code
           #:type-size #:byte #:ascii #:short #:long #:rational
           #:sbyte #:undefined #:sshort #:slong #:srational #:float #:double))
(in-package :climgio.tiff.core)

(defclass! tiff ()
  (bytes :type simple-vector)
  (ifds :type list)
  (endian :type endian))

(defmethod print-object ((object tiff) stream)
  (print-unreadable-object (object stream :type t)
    (let ((bytes (handler-case (bytes object) (unbound-slot nil)))
          (ifds (handler-case (ifds object) (unbound-slot nil))))
      (format stream "~<:BYTES ~a~:_ :IFDS ~a ~:>" (list bytes ifds)))))

(defclass! ifd ()
  ;; essential tag
  image-width image-length bits/sample
  compression photometric-interpretation
  strip-offsets rows/strip strip-byte-counts
  x-resolution y-resolution resolution-unit
  color-map

  ;; selective tag
  new-subfile-type thresholding
  cell-width cell-length fill-order
  image-description make model orientation
  samples/pixel min-sample-value
  max-sample-value planar-configuration
  gray-response-unit gray-response-curve
  software date-time artist host-computer
  extra-samples copyright

  ;; extended tag
  unknown-tag)

(defparameter *tiff-tags*
  '(;essential tags
    image-width image-length bits/sample
    compression photometric-interpretation
    strip-offsets rows/strip strip-byte-counts
    x-resolution y-resolution resolution-unit
    color-map

    ;; selective tags
    new-subfile-type thresholding
    cell-width cell-length fill-order
    image-description make model orientation
    samples/pixel min-sample-value
    max-sample-value planar-configuration
    gray-response-unit gray-response-curve
    software date-time artist host-computer
    extra-samples copyright

    ;; extended tag
    unknown-tag))

(defmethod print-object ((object ifd) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~<~{~{:~a ~a~}~:_~}~:>"
            (list (remove nil
                          (mapcar (lambda (entry)
                                    (handler-case (list entry (slot-value object entry))
                                      (unbound-slot nil)))
                                  *tiff-tags*))))))

(defun make-ifd (&rest initargs)
  (apply #'make-instance 'ifd initargs))

(defclass! ifd-entry ()
  (ifd-tag :type (unsigned-byte 16))
  (ifd-type :type (unsigned-byte 16))
  (ifd-count :type (unsigned-byte 32))
  (ifd-data :type (unsigned-byte 32)))

(defmethod print-object ((object ifd-entry) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~<:IFD-TAG ~a ~:_:IFD-TYPE ~a ~:_:IFD-COUNT ~a ~:_:IFD-DATA ~a~:>"
            (list (ifd-tag object) (ifd-type object) (ifd-count object) (ifd-data object)))))

(defun make-ifd-entry (&rest initargs)
  (apply #'make-instance 'ifd-entry initargs))

(defparameter *tiff-tag-table*
  '(;; essential tags
    (image-width . 256)
    (image-length . 257)
    (bits/sample . 258)
    (compression . 259)
    (photometric-interpretation . 262)
    (strip-offsets . 273)
    (rows/strip . 278)
    (strip-byte-counts . 279)
    (x-resolution . 282)
    (y-resolution . 283)
    (resolution-unit . 296)
    (color-map . 320)

    ;; selective tags
    (new-subfile-type . 254)
    (thresholding . 263)
    (cell-width . 264)
    (cell-length . 265)
    (fill-order . 266)
    (image-description . 270)
    (make . 271)
    (model . 272)
    (orientation . 274)
    (samples/pixel . 277)
    (min-sample-value . 280)
    (max-sample-value . 281)
    (planar-configuration . 290)
    (gray-response-unit . 290)
    (gray-response-curve . 291)
    (software . 305)
    (date-time . 306)
    (artist . 315)
    (host-computer . 316)
    (extra-samples . 338)
    (copyright . 33432)))

(defun tag-code (name)
  (aif (assoc name *tiff-tag-table*)
       (cdr it)
       0))

(defun tag-name (code)
  (aif (rassoc code *tiff-tag-table*)
       (car it)
       'unknown-tag))

(defparameter *tiff-type-table*
  '((byte . 1)
    (ascii . 2)
    (short . 3)
    (long . 4)
    (rational . 5)
    (sbyte . 6)
    (undefined . 7)
    (sshort . 8)
    (slong . 9)
    (srational . 10)
    (float . 11)
    (double . 12)))

(defun type-code (name)
  (aif (assoc name *tiff-type-table*)
       (cdr it)
       (error "Unknown type name: ~a" name)))

(defun type-name (code)
  (aif (rassoc code *tiff-type-table*)
       (car it)
       (error "Unknown type code: ~a" code)))

(defparameter *tiff-type-size-table*
  '((byte . 1)
    (ascii . 1)
    (short . 2)
    (long . 4)
    (rational . 8)
    (sbyte . 1)
    (sshort . 2)
    (slong . 4)
    (srational . 8)
    (float . 4)
    (double . 8)))

(defun type-size (name)
  (aif (assoc name *tiff-type-size-table*)
       (cdr it)
       (error "Unknown type name: ~a" name)))
