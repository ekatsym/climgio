(defpackage climgio.tiff.read
  (:use :common-lisp)
  (:import-from :climgio.tiff
                #:read-tiff)
  (:import-from :climgio.util
                #:index
                #:n-list?
                #:ensure-list
                #:proper-list->circular-list
                #:partial
                #:rpartial)
  (:import-from :climgio.binary-data
                #:endian
                #:big-endian
                #:little-endian
                #:read-all-bytes
                #:load-byte)
  (:import-from :climgio.tiff.core
                #:tiff
                #:bytes
                #:ifds
                #:*tiff-tags*
                #:ifd #:make-ifd
                #:image-width #:image-length #:bits/sample
                #:strip-offsets #:strip-byte-counts
                #:ifd-entry #:make-ifd-entry
                #:ifd-tag #:ifd-type #:ifd-count #:ifd-data
                #:tag-name #:tag-code
                #:type-name #:type-code
                #:type-size)
  (:export #:read-tiff
           #:read-all-bytes
           #:bytes->tiff
           #:tiff->images))
(in-package :climgio.tiff.read)

(defun read-tiff (stream)
  (let ((images (tiff->images (bytes->tiff (read-all-bytes stream)))))
    (if (n-list? 1 images)
        (first images)
        images)))

;;; bytes->tiff
(defun bytes->tiff (bytes)
  (check-type bytes simple-vector)
  (let* ((from-end (case (load-byte bytes 0 2 nil)
                     (19789 nil)
                     (18761 t)
                     (otherwise (error "Unknown endian."))))
         (version (load-byte bytes 2 2 from-end))
         (ifd-index (load-byte bytes 4 4 from-end)))
    (assert (= version 42) () 'error "This is not a TIFF image.")
    (make-instance 'tiff :bytes bytes
                         :ifds (parse-ifds bytes ifd-index from-end)
                         :endian (if from-end 'little-endian 'big-endian))))

(defun parse-ifds (bytes index from-end)
  (check-type bytes simple-vector)
  (check-type index index)
  (check-type from-end boolean)
  (do ((i index (load-byte bytes (+ i 2 (* 12 (load-byte bytes i 2 from-end))) 4
                           from-end))
       (acc '() (cons (parse-ifd bytes i from-end) acc)))
      ((zerop i) (nreverse acc))))

(defun parse-ifd (bytes index from-end)
  (check-type bytes simple-vector)
  (check-type index index)
  (check-type from-end boolean)
  (let ((ifd (make-ifd)))
    (dotimes (i (load-byte bytes index 2 from-end) ifd)
      (let ((ifd-tag   (load-byte bytes (+ index (* i 12) 2) 2 from-end))
            (ifd-type  (load-byte bytes (+ index (* i 12) 4) 2 from-end))
            (ifd-count (load-byte bytes (+ index (* i 12) 6) 4 from-end))
            (ifd-data  (load-byte bytes (+ index (* i 12) 10) 4 from-end)))
        (setf (slot-value ifd (tag-name ifd-tag))
              (make-ifd-entry :ifd-tag ifd-tag
                              :ifd-type ifd-type
                              :ifd-count ifd-count
                              :ifd-data ifd-data))))))

;;; tiff->images
(defun tiff->images (tiff)
  (check-type tiff tiff)
  (with-slots (bytes ifds endian) tiff
    (let ((images '()))
      (dolist (ifd ifds (nreverse images))
        (push (ifd->image ifd bytes (eq endian 'little-endian))
              images)))))

(defun ifd->image (ifd bytes from-end)
  (check-type ifd ifd)
  (check-type bytes simple-vector)
  (check-type from-end boolean)
  (let* ((height (ifd-data (image-length ifd)))
         (width (ifd-data (image-width ifd)))
         (channels (ifd-count (bits/sample ifd))))
    (copy-pixels-from-bytes-to-image
      (make-array (list height width channels))
      bytes
      (parse-ifd-entry-data-table bytes (strip-offsets ifd) from-end)
      (parse-ifd-entry-data-table bytes (strip-byte-counts ifd) from-end)
      (parse-ifd-entry-data-table bytes (bits/sample ifd) from-end)
      from-end)))

(defun copy-pixels-from-bytes-to-image (image bytes strip-indexs strip-sizes depths from-end)
  (check-type image (simple-array * (* * *)))
  (check-type bytes simple-vector)
  (check-type strip-indexs list)
  (check-type strip-sizes list)
  (check-type depths list)
  (check-type from-end boolean)
  (let ((height (array-dimension image 0))
        (width (array-dimension image 1))
        (channels (array-dimension image 2)))
    (labels ((rec (h w c i i0 size indexs sizes depths)
               (cond ((= h height) image)
                     ((= w width)
                      (rec (1+ h) 0 c i i0 size indexs sizes depths))
                     ((= c channels)
                      (rec h (1+ w) 0 i i0 size indexs sizes depths))
                     ((>= i size)
                      (rec h w c 0 (first indexs) (first sizes) (rest indexs) (rest sizes) depths))
                     (t
                      (setf (aref image h w c)
                            (load-byte bytes (+ i0 i) (first depths) from-end))
                      (rec h w (1+ c) (+ i (first depths)) i0 size indexs sizes (rest depths))))))
      (rec 0 0 0 0
           (first strip-indexs)
           (first strip-sizes)
           (rest strip-indexs)
           (rest strip-sizes)
           (proper-list->circular-list (mapcar (rpartial #'/ 8) depths))))))

(defun parse-ifd-entry-data-table (bytes ifd-entry from-end)
  (check-type bytes simple-vector)
  (check-type ifd-entry ifd-entry)
  (check-type from-end boolean)
  (with-slots (ifd-type ifd-count ifd-data) ifd-entry
    (let* ((type-size (type-size (type-name ifd-type)))
           (data-size (* type-size ifd-count)))
      (if (> data-size 4)
          (let ((acc '()))
            (dotimes (i ifd-count (nreverse acc))
              (push (load-byte bytes (+ ifd-data (* i type-size)) type-size from-end)
                    acc)))
          (list (ldb (byte 16 (- 32 (* data-size 8))) ifd-data))))))
#|
(with-open-file (file "img4.tif" :element-type '(unsigned-byte 8))
  (read-tiff file))
|#
