(defpackage climgio.tiff.write
  (:use :common-lisp)
  (:import-from :climgio.tiff
                #:write-tiff)
  (:import-from :climgio.util
                #:index
                #:n-list?
                #:ensure-list
                #:enum
                #:proper-list->circular-list
                #:append1
                #:partial
                #:rpartial)
  (:import-from :climgio.binary-data
                #:endian
                #:big-endian
                #:little-endian
                #:write-all-bytes
                #:load-byte)
  (:import-from :climgio.tiff.core
                #:tiff
                #:bytes
                #:ifds
                #:*tiff-tags*
                #:ifd #:make-ifd
                #:image-width #:image-length #:bits/sample
                #:compression #:photometric-interpretation
                #:strip-offsets #:rows/strip #:strip-byte-counts
                #:x-resolution #:y-resolution #:resolution-unit
                #:color-map
                #:ifd-entry #:make-ifd-entry
                #:ifd-tag #:ifd-type #:ifd-count #:ifd-data
                #:tag-name #:tag-code
                #:type-name #:type-code
                #:type-size #:byte #:ascii #:short #:long #:rational
                #:sbyte #:undefined #:sshort #:slong #:srational #:float #:double)
  (:export #:write-tiff
           #:images->tiff
           #:write-all-bytes))
(in-package :climgio.tiff.write)

(defun write-tiff (stream image &key (depth 8))
  (check-type stream stream)
  (assert (or (and (listp image)
                   (mapc (lambda (img) (typep img '(simple-array * (* * *)))) image))
              (typep image '(simple-array * (* * *)))))
  (assert (zerop (mod depth 8)))
  (write-all-bytes stream (bytes (images->tiff (ensure-list image) depth))))

(defparameter *ifd-entry-counts* 9)
(defparameter *ifd-size* (+ 2 (* 12 *ifd-entry-counts*) 4))

(defun images->tiff (images depth)
  (check-type images list)
  (mapc (lambda (img) (check-type img (simple-array * (* * *)))) images)
  (assert (zerop (mod depth 8)))
  (let* ((header-offset 0)
         (ifd1-offset 8)
         (ifd-offsets (nreverse
                        (rest
                          (reduce (lambda (acc img)
                                    (cons (+ *ifd-size*
                                             (image-size img depth)
                                             (ifd-option-size img)
                                             (first acc))
                                          acc))
                                  images
                                  :initial-value (list ifd1-offset)))))
         (image-offsets (mapcar (partial #'+ *ifd-size*) ifd-offsets))
         (ifd-option-offsets (mapcar (lambda (img img-offset)
                                       (+ img-offset (image-size img depth)))
                                     images
                                     image-offsets))
         (bytes (make-array (bytes-size images depth) :initial-element 0))
         (ifds (mapcar (lambda (img image-offset ifd-option-offset)
                         (image->ifd img depth image-offset ifd-option-offset))
                       images
                       image-offsets
                       ifd-option-offsets)))
    (set-bytes bytes
               images
               depth
               ifds
               header-offset
               ifd-offsets
               image-offsets
               ifd-option-offsets)
    (make-instance 'tiff
                   :bytes bytes
                   :ifds ifds
                   :endian 'big-endian)))

(defun image-size (image depth)
  (* (/ depth 8) (array-total-size image)))

(defun ifd-option-size (image)
  (let ((max-strip-size 8000)
        (channels (array-dimension image 2)))
    (+ (* 2 channels) ; bits/sample
       (* (ceiling (array-total-size image) max-strip-size)
          (type-size 'long)
          2) ; strip-offsets + strip-byte-counts
       (* 2 (type-size 'rational)))))

(defun bytes-size (images depth)
  (let* ((n (length images))
         (header-size 8))
    (+ header-size
       (* n *ifd-size*)
       (reduce (lambda (acc img)
                 (+ (image-size img depth) acc))
               images
               :initial-value 0)
       (reduce (lambda (acc img)
                 (+ (ifd-option-size img) acc))
               images
               :initial-value 0))))

(defun image->ifd (image depth image-offset ifd-option-offset)
  (check-type image (simple-array * (* * *)))
  (assert (= (mod depth 8) 0))
  (check-type image-offset index)
  (check-type ifd-option-offset index)
  (let* ((height (array-dimension image 0))
         (width (array-dimension image 1))
         (channels (array-dimension image 2))
         (max-strip-size 8000)
         (image-size (image-size image depth))
         (strip-counts (ceiling image-size max-strip-size))
         (bits/sample-offset ifd-option-offset)
         (strip-offsets-offset (+ bits/sample-offset (* channels 2)))
         (strip-byte-counts-offset (+ strip-offsets-offset (* (type-size 'long) strip-counts)))
         (x-resolution-offset (+ strip-byte-counts-offset (* (type-size 'long) strip-counts)))
         (y-resolution-offset (+ x-resolution-offset (type-size 'rational))))
    (make-ifd :image-width
              (make-ifd-entry :ifd-tag (tag-code 'image-width)
                              :ifd-type (type-code 'long)
                              :ifd-count 1
                              :ifd-data width)
              :image-length
              (make-ifd-entry :ifd-tag (tag-code 'image-length)
                              :ifd-type (type-code 'long)
                              :ifd-count 1
                              :ifd-data height)
              :bits/sample
              (make-ifd-entry :ifd-tag (tag-code 'bits/sample)
                              :ifd-type (type-code 'short)
                              :ifd-count channels
                              :ifd-data (if (= channels 1)
                                            depth
                                            bits/sample-offset))
              :photometric-interpretation
              (make-ifd-entry :ifd-tag (tag-code 'photometric-interpretation)
                              :ifd-type (type-code 'short)
                              :ifd-count 1
                              :ifd-data (ecase channels
                                          (1 1)
                                          (3 2)))
              :strip-offsets
              (make-ifd-entry :ifd-tag (tag-code 'strip-offsets)
                              :ifd-type (type-code 'long)
                              :ifd-count strip-counts
                              :ifd-data (if (= strip-counts 1)
                                            image-offset
                                            strip-offsets-offset))
              :rows/strip
              (make-ifd-entry :ifd-tag (tag-code 'rows/strip)
                              :ifd-type (type-code 'long)
                              :ifd-count 1
                              :ifd-data (ceiling (min max-strip-size image-size) width))
              :strip-byte-counts
              (make-ifd-entry :ifd-tag (tag-code 'strip-byte-counts)
                              :ifd-type (type-code 'long)
                              :ifd-count strip-counts
                              :ifd-data (if (= strip-counts 1)
                                            image-size
                                            strip-byte-counts-offset))
              :x-resolution
              (make-ifd-entry :ifd-tag (tag-code 'x-resolution)
                              :ifd-type (type-code 'rational)
                              :ifd-count 1
                              :ifd-data x-resolution-offset)
              :y-resolution
              (make-ifd-entry :ifd-tag (tag-code 'y-resolution)
                              :ifd-type (type-code 'rational)
                              :ifd-count 1
                              :ifd-data y-resolution-offset))))

(defun set-bytes (bytes images depth ifds header-offset ifd-offsets image-offsets ifd-option-offsets)
  ;; set header
  (setf (load-byte bytes (+ header-offset 0) 2 nil) #x4d4d
        (load-byte bytes (+ header-offset 2) 2 nil) 42
        (load-byte bytes (+ header-offset 4) 4 nil) (first ifd-offsets))

  ;; set each of ifds
  (mapc (lambda (img ifd ifd-offset next-ifd-offset img-offset ifd-option-offset)
          (set-ifd bytes img ifd ifd-offset next-ifd-offset img-offset ifd-option-offset depth))
        images
        ifds
        ifd-offsets
        (append1 (rest ifd-offsets) 0)
        image-offsets
        ifd-option-offsets))

(defun set-ifd (bytes image ifd ifd-offset next-ifd-offset image-offset ifd-option-offset depth)
  (set-ifd-entry-counts bytes ifd-offset)
  (set-ifd-entries bytes ifd ifd-offset)
  (set-next-ifd-pointer bytes ifd-offset next-ifd-offset)
  (set-image bytes image image-offset depth)
  (set-ifd-options bytes image ifd depth image-offset ifd-option-offset))

(defun set-ifd-entry-counts (bytes ifd-offset)
  (setf (load-byte bytes (+ ifd-offset 0) 2 nil)
        *ifd-entry-counts*))

(defun set-ifd-entries (bytes ifd ifd-offset)
  (with-slots (image-width image-length bits/sample
               photometric-interpretation strip-offsets
               rows/strip strip-byte-counts
               x-resolution y-resolution) ifd
    (mapc (lambda (ifd-entry offset)
            (with-slots (ifd-tag ifd-type ifd-count ifd-data) ifd-entry
              (assert (<= 0 ifd-data (1- (expt 256 4))))
              (let ((data-size (min 4 (* (type-size (type-name ifd-type))
                                         ifd-count))))
                (setf (load-byte bytes (+ offset 0) 2 nil) ifd-tag
                      (load-byte bytes (+ offset 2) 2 nil) ifd-type
                      (load-byte bytes (+ offset 4) 4 nil) ifd-count
                      (load-byte bytes (+ offset 8) data-size nil) ifd-data))))
          (list image-width image-length bits/sample
                photometric-interpretation strip-offsets
                rows/strip strip-byte-counts
                x-resolution y-resolution)
          (enum *ifd-entry-counts* :start (+ ifd-offset 2) :step 12))))

(defun set-next-ifd-pointer (bytes ifd-offset next-ifd-offset)
  (setf (load-byte bytes (+ ifd-offset 2 (* 12 *ifd-entry-counts*)) 4 nil)
        next-ifd-offset))

(defun set-image (bytes image image-offset depth)
  (let ((height (array-dimension image 0))
        (width (array-dimension image 1))
        (channels (array-dimension image 2))
        (pixel-size (/ depth 8)))
    (dotimes (h height)
      (dotimes (w width)
        (dotimes (c channels)
          (let ((i (+ c (* w channels) (* h width channels))))
            (setf (load-byte bytes (+ image-offset (* i pixel-size)) pixel-size nil)
                  (aref image h w c))))))))

(defun set-ifd-options (bytes image ifd depth image-offset ifd-option-offset)
  (let* ((channels (array-dimension image 2))
         (max-strip-size 8000)
         (image-size (image-size image depth))
         (strip-counts (ceiling image-size max-strip-size))

         (bits/sample-offset ifd-option-offset)
         (bits/sample-size (* channels 2))

         (strip-offsets-offset (+ bits/sample-offset bits/sample-size))
         (strip-offsets-size (* (type-size 'long) strip-counts))

         (strip-byte-counts-offset (+ strip-offsets-offset strip-offsets-size))
         (strip-byte-counts-size strip-offsets-size)

         (x-resolution-offset (+ strip-byte-counts-offset strip-byte-counts-size))
         (x-resolution-size/2 (/ (type-size 'rational) 2))

         (y-resolution-offset (+ x-resolution-offset (* x-resolution-size/2 2)))
         (y-resolution-size/2 x-resolution-size/2))
    (with-slots (bits/sample strip-offsets strip-byte-counts x-resolution y-resolution) ifd
      (unless (= channels 1)
        (dotimes (i channels)
          (setf (load-byte bytes (+ bits/sample-offset (* i 2)) 2 nil)
                depth)))
      (unless (= strip-counts 1)
        (dotimes (i strip-counts)
          (setf (load-byte bytes
                           (+ strip-offsets-offset (* i (type-size 'long)))
                           strip-offsets-size
                           nil)
                (+ image-offset (* i max-strip-size)))
          (setf (load-byte bytes
                           (+ strip-byte-counts-offset (* i (type-size 'long)))
                           strip-byte-counts-size
                           nil)
                (if (= i (1- strip-counts))
                    (mod image-size max-strip-size)
                    max-strip-size))))
      (setf (load-byte bytes x-resolution-offset x-resolution-size/2 nil) 400
            (load-byte bytes (+ x-resolution-offset x-resolution-size/2) x-resolution-size/2 nil) 2
            (load-byte bytes y-resolution-offset y-resolution-size/2 nil) 200
            (load-byte bytes (+ y-resolution-offset y-resolution-size/2) y-resolution-size/2 nil) 1))))

#|
(let ((img1 (make-array (list 3 3 1) :initial-contents '(((000) (255) (000))
                                                         ((255) (255) (255))
                                                         ((255) (255) (000)))))
      (img2 (make-array (list 3 3 3) :initial-contents '(((255 000 000) (255 255 255) (000 255 000))
                                                         ((255 255 255) (255 255 255) (255 255 255))
                                                         ((255 255 255) (255 255 255) (000 000 255))))))
  (format t "~&~a~%~a~%"
          img1
          (climgio.tiff.read:tiff->images (print (images->tiff (list img1) 8))))
  (terpri)
  (format t "~&~a~%~a~%"
          img2
          (climgio.tiff.read:tiff->images (images->tiff (list img2) 8)))
  (terpri)
  (format t "~&~a~%~a~%"
          (list img1 img2)
          (climgio.tiff.read:tiff->images (images->tiff (list img1 img2) 8))))

|#
