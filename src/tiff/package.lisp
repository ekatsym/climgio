(defpackage climgio.tiff
  (:export #:read-tiff
           #:write-tiff))
